from os import getenv
from os.path import dirname, isfile, join

from flask_cors import CORS

from dotenv import load_dotenv

_ENV_FILE = join(dirname(__file__),'.env')

if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)

from apps import  create_app

app = create_app(getenv('FLASK_ENV') or 'default')

CORS(app)
if __name__ == '__main__':
    ip = '127.0.0.1'
    port = app.config['APP_PORT']
    debug = app.config['DEBUG'] 

    #ejecuta el servidor en web flask
    app.run(
        host=ip, debug=debug, port=port, use_reloader=debug
    )