from os import getenv

class Config:
    SECRET_KEY = getenv('SECRET_KEY') or 'un string ramdon gigante'
    APP_PORT = int(getenv('APP_PORT'))
    MONGODB_HOST = getenv('MONGODB_URI')
    DEBUG = eval(getenv('DEBUG').title())

class DevelopmentConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True

class TestingConfig(Config):
    FLASK_ENV = 'testing'
    TESTING = True
    MONGODB_HOST = getenv('MONGODB_URI_TEST')

config = {
    'development': DevelopmentConfig,
    'testing':TestingConfig,
    'default': DevelopmentConfig
}

