from marshmallow import Schema
from marshmallow.fields import Str,Boolean, Nested
from apps.messages import MSG_FIELD_REQUIRED

class LabelRegistrationSchema(Schema):
    name = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    symbol = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    description = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})

class LabelSchema(Schema):
    id = Str(required=False, error_messages={'required':MSG_FIELD_REQUIRED})
    name = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    symbol = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    description = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    active = Boolean() 

class LabelDetailRegistrationSchema(Schema):
    id_label = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    treatment = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    url = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})

class LabelDetailSchema(Schema):
    id = Str(required=False, error_messages={'required':MSG_FIELD_REQUIRED})
    id_label = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    treatment = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})
    url = Str(required=True, error_messages={'required':MSG_FIELD_REQUIRED})

class LabelUpdateSchema(Schema):
    name = Str()
    symbol = Str()
    description = Str()

class LabelDetailUpdateSchema(Schema):
    id_label = Str()
    treatment = Str()
    url = Str()   