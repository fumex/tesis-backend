#Third
from mongoengine.errors import FieldDoesNotExist, DoesNotExist, MultipleObjectsReturned

#Apps
from apps.responses import resp_exception, resp_does_not_exist

#local
from .models import Label, LabelDetail

def get_label_by_id(label_id: str):
    try:
        #buscamos todos los usaurios de la base de datos
        return Label.objects.get(id=label_id)

    except DoesNotExist:
        return resp_does_not_exist('Labels','etiquetas')
    
    except FieldDoesNotExist as e:
        return resp_exception('Labels',description=e.__str__())
    
    except Exception as e:
        return resp_exception('Labels', description=e.__str__())

def get_label_detail_by_id(detail_id:str):
    try:
        #buscamos todos los usaurios de la base de datos
        return LabelDetail.objects.get(id=detail_id)

    except DoesNotExist:
        return resp_does_not_exist('LabelsDetail','deatlle etiquetas')
    
    except FieldDoesNotExist as e:
        return resp_exception('LabelsDetail',description=e.__str__())
    
    except Exception as e:
        return resp_exception('LabelsDetail', description=e.__str__())

def exists_name_in_label(name:str,  isinstance=None):
    """
    Verificamos si existe un etiqueta con ese nombre 
    """
    label = None

    try:
        label = Label.objects.get(name=name)
    
    except DoesNotExist:
        return False

    except MultipleObjectsReturned:
        return True
    
    # verifico  si el id retornado en la busqueda es el mismo
    # de la instanca 
    if isinstance and isinstance.id == label.id:
        return False
    
    return True

def get_label_by_name(name:str):
    try:
        return Label.objects.get(name=name)

    except DoesNotExist:
        return resp_does_not_exist('Labels', 'Etiquetas')

    except FieldDoesNotExist as e:
        return resp_exception('Labels', description=e.__str__())
    
    except Exception as e:
        return resp_exception('Labels', description=e.__str__())

