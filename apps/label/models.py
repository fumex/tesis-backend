from datetime import datetime

from mongoengine import (
    BooleanField,
    DateTimeField,
    DictField,
    EmailField,
    EmbeddedDocument,
    EmbeddedDocumentField,
    StringField,
    URLField
)

from apps.db import db

class LabelMixin(db.Document):
    
    """
    implementacion por defecto para campos de label
    """
    meta = {
        'abstract':True,
        'ordering':['symbol']
    }

    symbol = StringField(required=True, max_length=5)
    description = StringField(required=True)
    created = DateTimeField(default=datetime.now)
    active = BooleanField(default=False)

    def is_activate(self):
        return self.active

class Label(LabelMixin):
    meta = {'collection':'labels'}

    name = StringField(required=True)

class LabelDetailMixin(db.Document):
    """
    Implementacion de campos por defecto LabelDetail
    """
    meta = {
        'abstract':True,
        'ordering':['url']
    }
    treatment = StringField(required=True)
    url = StringField(required=True)

class LabelDetail(LabelDetailMixin):
    meta = { 'collection':'labels_detail'}
    id_label = StringField(required=True)