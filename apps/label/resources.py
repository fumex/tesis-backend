#Flask
from flask import request

#third
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist
from mongoengine.errors import NotUniqueError, ValidationError

#Apps
from apps.responses import resp_ok, resp_exception, resp_data_invalid, resp_already_exists

from apps.messages import MSG_RESOURCE_FETCHED_PAGINATED, MSG_RESOURCE_FETCHED
from apps.messages import MSG_NO_DATA, MSG_RESOURCE_UPDATED, MSG_INVALID_DATA
from apps.messages import MSG_ALREADY_EXISTS, MSG_RESOURCE_DELETED, MSG_RESOURCE_CREATED

#Local
from .models import Label, LabelDetail
from .schema import LabelSchema, LabelUpdateSchema,  LabelRegistrationSchema, LabelDetailSchema, LabelDetailRegistrationSchema, LabelDetailUpdateSchema
from .utils import get_label_by_id, exists_name_in_label, get_label_detail_by_id

class LabelList(Resource):
    
    def get(self):
        #iniciamos el esquema podiendo contener varios objetos
        schema = LabelSchema(many = True)
        
        try:
            #buscamos todos los usuario en la base de datos 
            labels = Label.objects()
        except FieldDoesNotExist as e:
            return resp_exception('Labels', description=e.__str__())
        except Exception as e:
            return resp_exception('Labels', description=e.__str__())
        
        #hacemos un dum de los objetos consultados
        result = schema.dump(labels)
        return resp_ok(
            'Label',MSG_RESOURCE_FETCHED_PAGINATED.format('Labels'),data=result.data
        )

class LabelCreate(Resource):
    
    def post(self, *args, **kwargs):
        req_data = request.get_json() or None
        data, errors, result = None, None, None
        schema = LabelRegistrationSchema()

        if req_data is None:
            return resp_data_invalid('Labels',[],msg=MSG_NO_DATA)
        
        data, errors = schema.load(req_data)
        
        if errors:
            return resp_data_invalid('Labels',errors)
        
        try:
            model = Label(**data)
            model.save()
        
        except NotUniqueError:
            return resp_already_exists('Labels','etiqueta')
        
        except ValidationError as e:
            return resp_exception('Labels', msg=MSG_INVALID_DATA, description=e)
        
        except Exception as e:
            return resp_exception('Labels', description=e) 
        
        schema = LabelSchema()
        result = schema.dump(model)

        return resp_ok(
            'Labels', MSG_RESOURCE_CREATED.format('Labels'), data=result.data
        )

class LabelResources(Resource):
    
    def get(self, label_id):
        result = None
        schema = LabelSchema()
        label = get_label_by_id(label_id)

        if not isinstance(label, Label):
            return label
        
        result = schema.dump(label)

        return resp_ok(
            'Labels', MSG_RESOURCE_FETCHED.format('Etiquetas'), data=result.data
        )
    
    def post(self, label_id):
        result = None
        schema = LabelSchema()
        update_schema = LabelUpdateSchema()
        req_data = request.get_json() or None
        name = None
        
        #Validar si esta basio
        if req_data is None:
            return resp_data_invalid('Labels',[], msg=MSG_NO_DATA)
        
        #busco la etiqueta de la coleccion con id
        label = get_label_by_id(label_id)

        if not isinstance(label, Label):
            return label

        #cargamos mis datos de acuerdo con el schema de actualizacion
        data, errors = update_schema.load(req_data)
        #en caso de error retorno una respuesta 422 con los errores de validacion de schema
        if errors:
            return resp_data_invalid('Labels',errors)
        
        name  = data.get('name',None)
        # valida si existe un nombre en la coleccion de etiquetas
        if name and exists_name_in_label(name, label):
            return resp_data_invalid(
                'Labels',[{'email':[MSG_ALREADY_EXISTS.format('etiquetas')]}]
            )
        
        try:
            #para cada llave dentro de los datos de esquema update
            #atribuimos su valor 
            for i in data.keys():
                label[i]=data[i]
            label.save()

        except NotUniqueError:
            return resp_already_exists('Labels', 'etiquetas')

        except Exception as e:
            return resp_exception('Labels', description=e.__str__())
        
        result = schema.dump(label)

        return resp_ok(
            'Label',MSG_RESOURCE_UPDATED.format('Etiqueta'), data=result.data
        )
    
    

class LabelDelete(Resource):
    def get(self, label_id):
        #Busco la etiqueta en la coleccion por su id 
        label = get_label_by_id(label_id)

        #si no es una instancia del modelo usuario retorna una respuesta
        if not isinstance(label, Label):
            return label
        try:
            label.delete()
        
        except NotUniqueError:
            return resp_already_exists('Label','etiqueta')

        except ValidationError as e:
            return resp_exception('Label', msg=MSG_INVALID_DATA, description=e.__str__())
        
        return resp_ok(
            'Label', MSG_RESOURCE_DELETED.format('etiqueta')
        )
class LabelDetailCreate(Resource):
    def post(self, *args, **kwargs):
        req_data = request.get_json() or None
        data, errors, result = None, None, None
        schema = LabelDetailRegistrationSchema()

        if req_data is None:
            return resp_data_invalid('LabelsDetail',[], msg=MSG_NO_DATA)
        
        data, errors = schema.load(req_data)

        if errors:
            return resp_data_invalid('LabelsDetail', errors)

        try:
            model = LabelDetail(**data)
            model.save()

        except NotUniqueError:
            return resp_already_exists('LabelsDetail','detalle_etiqueta')
        
        except ValidationError as e:
            return resp_exception('LabelsDetail',msg=MSG_INVALID_DATA, description=e)

        except Exception as e:
            return resp_exception('LabelsDetail', description=e)

        schema = LabelDetailSchema()
        result = schema.dump(model)

        return resp_ok(
            'LabelsDetail', MSG_RESOURCE_FETCHED.format('Etiquetas'), data=result.data
        )
        
class LabelDetailList(Resource):
    def get(self, id_label):
        result = None
        schema = LabelDetailSchema(many = True)
        
        try:
            label_details = LabelDetail.objects(id_label=id_label)

        except FieldDoesNotExist as e:
            return resp_exception('LabelsDetail', description=e.__str__())

        except Exception as e:
            return resp_exception('LabelsDetail', description=e.__str__())
        
        #hacemos un dum
        result=schema.dump(label_details)
        return resp_ok(
            'labelDetail', MSG_RESOURCE_FETCHED_PAGINATED.format('LabelsDetail'), data=result.data
        )

class DetailList(Resource):
    def get(self):
        result = None
        schema = LabelDetailSchema(many = True)
        try:
            label_details = LabelDetail.objects()
        except FieldDoesNotExist as e:
            return resp_exception('LabelsDetail', description=e.__str__())
        except Exception as e:
            return resp_exception('LabelsDetail', description=e.__str__())
        
        #hacemos un dump
        result = schema.dump(label_details)
        return resp_ok(
            'labelDetail', MSG_RESOURCE_FETCHED_PAGINATED.format('LabelsDetail'), data=result.data
        )

class LabelDetailResources(Resource):

    def post(self, id_label):
        result = None
        schema = LabelDetailSchema()
        update_schema = LabelDetailUpdateSchema()
        req_data = request.get_json() or None

        # validar si esta basio
        if req_data is None:
            return resp_data_invalid('LabelsDetail',[],msg=MSG_NO_DATA)

        #buscamos la etiqueta  de la colecion con id 
        label_detail = get_label_detail_by_id(id_label)

        if not isinstance(label_detail,LabelDetail):
            return label_detail

        #cargamos los daos deacuerdo con el schema
        data, errors = update_schema.load(req_data)
        
        if errors:
            return resp_data_invalid('LabelsDetail', errors)

        try:
            #para cada llave dentro de los datos  del schema update  
            # atribuimos su valor
            for i in data.keys():
                label_detail[i]=data[i]

            label_detail.save()

        except NotUniqueError:
            return resp_already_exists('LabelsDetail','Detalles de etiquetas') 
        except Exception as e:
            return resp_exception('LabelsDetail',description=e.__str__())

        result = schema.dump(label_detail)
        
        return resp_ok(
            'label',MSG_RESOURCE_UPDATED.format('Etiqueta Detalle'), data=result.data
        )
    
    def get(self,id_label):
        detail = get_label_detail_by_id(id_label)

        if not isinstance(detail, LabelDetail):
            return detail
        try:
            detail.delete()
        
        except NotUniqueError:
            return resp_already_exists('Detail','Detalle')
        except ValidationError as e:
            return resp_exception('Detail',msg=MSG_INVALID_DATA, description=e.__str__())
        
        return resp_ok(
            'Detail', MSG_RESOURCE_DELETED.format('Detail')
        )


        