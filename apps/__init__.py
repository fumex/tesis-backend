from flask import Flask
from config import config

#Realizamos importtacion de funciones
from .api import configure_api
from .db import db

def create_app(config_name):
    app = Flask('api-palta')

    app.config.from_object(config[config_name])
    #configuracion de mongoEngime
    db.init_app(app)
    #ejecutar la funcion de configuracion
    configure_api(app)

    return app
