from flask_restful import Api, Resource

#App
from apps.label.resources import LabelList, LabelResources, LabelCreate, LabelDetailCreate, LabelDetailList, LabelDetailResources
from apps.label.resources import LabelDelete, DetailList
from apps.prediction.resources import Prediccion, UploadPhoto

class Index(Resource):
    def get(self):
        return {'hello':'APIREST PALTA NUTRIENTES '}

api = Api()
def configure_api(app):
    #addicionar ruta
    api.add_resource(Index,'/')
    #crear Etiqueta
    api.add_resource(LabelCreate,'/label')
    # update, get
    api.add_resource(LabelResources,'/label/<string:label_id>')
    # Listar
    api.add_resource(LabelList,'/lista_labels')
    # delete 
    api.add_resource(LabelDelete,'/delete/<string:label_id>') 


    #crear detalle etiqueta
    api.add_resource(LabelDetailCreate,'/label/detail')

    #listar id
    api.add_resource(LabelDetailList,'/label/details/<string:id_label>')
    #listar 
    api.add_resource(DetailList,'/label/details')
    #update ,eliminar detalle etiqueta
    api.add_resource(LabelDetailResources,'/details/<string:id_label>')
    #upload Archivo
    api.add_resource(UploadPhoto,'/upload')
    #Prediccion 
    api.add_resource(Prediccion,'/prediccion')

    #inicializacion de api
    api.init_app(app)