#flask
from flask import request

#third
from flask_restful import Resource, Api, reqparse
import werkzeug

#Apps
from apps.responses import resp_ok, resp_exception, resp_data_invalid, resp_already_exists

from apps.messages import MSG_RESOURCE_FETCHED_PAGINATED, MSG_RESOURCE_FETCHED
from apps.messages import MSG_NO_DATA, MSG_RESOURCE_UPDATED, MSG_INVALID_DATA
from apps.messages import MSG_ALREADY_EXISTS, MSG_RESOURCE_DELETED, MSG_RESOURCE_CREATED

#Local
from .utils import predic_model

class Prediccion(Resource):
    def get(self):
        predi = predic_model()
        return resp_ok(
            'prediccion',MSG_RESOURCE_FETCHED.format('Predicciones '), data=predi
        )

class UploadPhoto(Resource):
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('file', type=werkzeug.datastructures.FileStorage, location='files')
        args = parse.parse_args()
        audioFile = args['file']
        audioFile.save("./apps/prediction/storage/tests/test.jpg")