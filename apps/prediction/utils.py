import cv2
import os
import marshal
import numpy as np
import tensorflow as tf
from  tensorflow.python.keras.models import load_model
from time import time

def preprocessing(filepath,size):
    test_img = []
    img_array = cv2.imread(filepath)
    img_resize = cv2.resize(img_array,(size,size))
    img_rgb = cv2.cvtColor(img_resize, cv2.COLOR_BGR2RGB)
    test_img.append(img_rgb)
    X = np.array(test_img)
    test_x = X/255.0
    return test_x

def predic_model():
    
    model1 = loadModel('InceptionB') 
    model2 = loadModel('DenseNetB')
    model3 = loadModel('MobileNetA')
    
    #predecir
    start_time1 = time()
    valor1 = prediction(model1,229)
    elapsed_time1 = time() - start_time1

    start_time2 = time()
    valor2 = prediction(model2,224)
    elapsed_time2 = time() - start_time2

    start_time3 = time()
    valor3 = prediction(model3,224)
    elapsed_time3 = time() - start_time3
    tiempo=[]
    tiempo.append(elapsed_time1)
    tiempo.append(elapsed_time2)
    tiempo.append(elapsed_time3)
    #print (tiempo) 
    respuesta = valor1 + valor2 + valor3 + [tiempo]
    print (respuesta)

    return respuesta

def loadModel(name):
    ruta = os.getcwd()  + '/apps/prediction/storage'
    model = load_model(ruta+ '/Modelo/' + name)
    return model

def prediction(model ,size):
    # cargamos el model
    ruta = os.getcwd()  + '/apps/prediction/storage'
    predi = model.predict(preprocessing(ruta + '/tests/test.jpg',size))*100
    #print(predi)
    return predi.tolist()
